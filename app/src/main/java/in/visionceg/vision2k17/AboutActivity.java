package in.visionceg.vision2k17;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import in.visionceg.vision2k17.Individuals.MainActivity;

public class AboutActivity extends AppCompatActivity {

    private ImageView  fb,tw,quo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
        fb = (ImageView) findViewById(R.id.fb_btn);
        tw = (ImageView) findViewById(R.id.twitter_btn);
        quo = (ImageView) findViewById(R.id.quora_btn);
        fb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.facebook.com/visionceg"));
                startActivity(browserIntent);

            }
        });

        tw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://twitter.com/Vision_CEG"));
                startActivity(browserIntent);

            }
        });
        quo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.quora.com/topic/Vision-CEG"));
                startActivity(browserIntent);

            }
        });

    }
}

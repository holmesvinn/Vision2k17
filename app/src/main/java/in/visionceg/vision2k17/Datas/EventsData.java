package in.visionceg.vision2k17.Datas;

import android.graphics.drawable.Drawable;

/**
 * Created by holmesvinn on 22/2/17.
 */
public class EventsData {

    private String eventname, category, description, rules, venue, contact, imageurl;
    private int imageid_like;

    public int getImageid_like() {
        return imageid_like;
    }

    public void setImageid_like(int imageid_like) {
        this.imageid_like = imageid_like;
    }

    public EventsData() {

    }



    public String getEventname() {
        return eventname;
    }

    public void setEventname(String eventname) {
        this.eventname = eventname;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getRules() {
        return rules;
    }

    public void setRules(String rules) {
        this.rules = rules;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getVenue() {
        return venue;
    }

    public void setVenue(String venue) {
        this.venue = venue;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getImageurl() {
        return imageurl;
    }

    public void setImageurl(String imageurl) {
        this.imageurl = imageurl;
    }
}

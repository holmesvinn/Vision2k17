package in.visionceg.vision2k17.Datas;

import android.media.Image;

/**
 * Created by holmesvinn on 17/2/17.
 */

public class Home {

    private String Title;
    private int imageid;

    public Home() {

    }

    public Home(int imageid, String Title) {
        this.imageid = imageid;
        this.Title = Title;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public int getImageid() {
        return imageid;
    }

    public void setImageid(int imageid) {
        this.imageid = imageid;
    }
}

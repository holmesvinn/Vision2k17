package in.visionceg.vision2k17.Datas;

/**
 * Created by holmesvinn on 26/2/17.
 */

public class UpdatesData  {

    private  String name,person,time,phone;

    public UpdatesData()
    {

    }

    public String getPerson() {
        return person;
    }

    public void setPerson(String person) {
        this.person = person;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}

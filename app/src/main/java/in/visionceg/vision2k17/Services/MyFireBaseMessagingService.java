package in.visionceg.vision2k17.Services;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.icu.util.ULocale;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.thefinestartist.finestwebview.FinestWebView;

import in.visionceg.vision2k17.Individuals.MainActivity;
import in.visionceg.vision2k17.R;
import io.paperdb.Paper;

/**
 * Created by holmesvinn on 22/2/17.
 */

public class MyFireBaseMessagingService extends FirebaseMessagingService {


    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {


        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        intent.putExtra("hello", "value");
        intent.addCategory(intent.CATEGORY_LAUNCHER);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP );
        PendingIntent pendingIntent = null;
        pendingIntent = PendingIntent.getActivity(this, 0, intent,PendingIntent.FLAG_UPDATE_CURRENT|PendingIntent.FLAG_ONE_SHOT);

//this,0,intent,PendingIntent.FLAG_ONE_SHOT
        Bitmap bmp  = BitmapFactory.decodeResource(getApplicationContext().getResources(),R.drawable.ic_stat_name);

        NotificationCompat.Builder notificationBuilder = new
                NotificationCompat.Builder(this);
                notificationBuilder.setSmallIcon(getNotificationIcon(notificationBuilder));
        notificationBuilder.setLargeIcon(bmp);


        notificationBuilder.setContentTitle("VISION CEG");
        notificationBuilder.setContentText(remoteMessage.getNotification().getBody());
        notificationBuilder.setAutoCancel(true);
        notificationBuilder.setDefaults(Notification.DEFAULT_VIBRATE);
        notificationBuilder.setContentIntent(pendingIntent);
        NotificationManager notificationManager =
                (NotificationManager)
                        getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0, notificationBuilder.build());
    }

    private int getNotificationIcon(NotificationCompat.Builder notificationBuilder ) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            int color = 0x008888;
            notificationBuilder.setColor(color);
            return R.drawable.ic_stat_name;

        } else {
            return R.mipmap.ic_launcher;
        }
    }


}

package in.visionceg.vision2k17.Services;

import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

/**
 * Created by holmesvinn on 22/2/17.
 */

public class MyFireBaseInstanceIdService extends FirebaseInstanceIdService {
    private final static String REG_TOKEN = "REG_TOKEN";
    @Override
    public void onTokenRefresh() {
        String Recent_Token = FirebaseInstanceId.getInstance().getToken();
        Log.d(REG_TOKEN,Recent_Token);
    }
}

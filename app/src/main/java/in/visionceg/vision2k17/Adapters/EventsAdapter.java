package in.visionceg.vision2k17.Adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import java.util.List;

import in.visionceg.vision2k17.Datas.EventsData;
import in.visionceg.vision2k17.Datas.UpdatesData;
import in.visionceg.vision2k17.Individual.TabEvents;
import in.visionceg.vision2k17.R;

import static in.visionceg.vision2k17.R.*;

/**
 * Created by holmesvinn on 22/2/17.
 */
public class EventsAdapter extends RecyclerView.Adapter<EventsAdapter.CustomViewHolder> {


    List<EventsData> eventsDatas;
    Context context;



    ImageView imageView;


    public EventsAdapter(Context context, List<EventsData> eventsDatas) {
        this.eventsDatas = eventsDatas;
        this.context = context;

    }



    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {




        DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder()

                .cacheInMemory(true)
                .cacheOnDisk(true)

                .build();
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context.getApplicationContext())

                .defaultDisplayImageOptions(defaultOptions)

                .build();
        ImageLoader.getInstance().init(config);

        View view = LayoutInflater.from(parent.getContext()).inflate(layout.events_list_row, null);
        CustomViewHolder viewHolder = new CustomViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final CustomViewHolder holder, int position) {

        final EventsData eventsData = eventsDatas.get(position);


        ImageLoader.getInstance().displayImage(eventsData.getImageurl(), holder.imageView, new ImageLoadingListener() {

            @Override
            public void onLoadingStarted(String imageUri, View view) {
                holder.imageView.setImageResource(drawable.nonet);
            }

            @Override
            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {

            }

            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {

                holder.imageView.setImageBitmap(loadedImage);

            }

            @Override
            public void onLoadingCancelled(String imageUri, View view) {

            }
        });

        //Setting text view title
        holder.textView.setText(Html.fromHtml(eventsData.getEventname()));
        holder.category.setText(String.valueOf(eventsData.getCategory()));







    }

    @Override
    public int getItemCount() {
        return (null != eventsDatas ? eventsDatas.size() : 0);
    }


    public class CustomViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private ImageView imageView;
        private TextView textView;
        private TextView category;





        public CustomViewHolder(View itemView) {
            super(itemView);
            this.imageView = (ImageView) itemView.findViewById(id.thumbnail);
            this.textView = (TextView) itemView.findViewById(id.title);
            this.category = (TextView) itemView.findViewById(id.text_category);
            imageView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {


            int positon = getAdapterPosition();
            String name = eventsDatas.get(positon).getEventname();
            String category = eventsDatas.get(positon).getCategory();
            String description = eventsDatas.get(positon).getDescription();
            String rules = eventsDatas.get(positon).getRules();
            String venue = eventsDatas.get(positon).getVenue();
            String contact = eventsDatas.get(positon).getContact();
            String imageurl = eventsDatas.get(positon).getImageurl();
            Intent intetn = new Intent(context.getApplicationContext(),TabEvents.class);
            intetn.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            Bundle b = new Bundle();
            b.putString("name", name);
            b.putString("category", category);
            b.putString("description", description);
            b.putString("rules", rules);
            b.putString("venue", venue);
            b.putString("contact", contact);
            b.putString("imageurl", imageurl);
            intetn.putExtras(b);
            context.getApplicationContext().startActivity(intetn);

        }
    }
}

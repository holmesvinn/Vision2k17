package in.visionceg.vision2k17.Adapters;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.List;

import in.visionceg.vision2k17.Datas.UpdatesData;
import in.visionceg.vision2k17.R;

/**
 * Created by holmesvinn on 26/2/17.
 */

public class UpdatesAdapter extends RecyclerView.Adapter<UpdatesAdapter.CustomViewHolder> {


    List<UpdatesData> updatesDatas;
    Context context;


    public UpdatesAdapter(Context context,List<UpdatesData> updatesDatas)
    {
        this.context = context;
        this.updatesDatas = updatesDatas;
    }
    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {


        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.updates_list_row, null);
        UpdatesAdapter.CustomViewHolder viewHolder = new UpdatesAdapter.CustomViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(CustomViewHolder holder, int position) {


         final UpdatesData updatesData = updatesDatas.get(position);
        holder.name.setText(updatesData.getName());
        holder.time.setText(updatesData.getTime());
        holder.person.setText(updatesData.getPerson());
        holder.call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String phone = updatesData.getPhone().toString();
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse(phone));
                context.startActivity(intent);
            }
        });



    }

    @Override
    public int getItemCount() {
        return (null != updatesDatas ? updatesDatas.size() : 0);
    }

    public class CustomViewHolder extends RecyclerView.ViewHolder {


        private TextView name,time,person;
        private ImageButton call;
        public CustomViewHolder(View itemView) {
            super(itemView);
            person = (TextView)itemView.findViewById(R.id.update_person);
            name = (TextView) itemView.findViewById(R.id.update_name);
            time = (TextView) itemView.findViewById(R.id.time_update);
            call = (ImageButton) itemView.findViewById(R.id.phone_update_button);

        }
    }
}

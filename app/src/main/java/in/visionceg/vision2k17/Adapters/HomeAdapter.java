package in.visionceg.vision2k17.Adapters;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import in.visionceg.vision2k17.Individuals.Accomodation;
import in.visionceg.vision2k17.Individuals.Contacts;
import in.visionceg.vision2k17.Individuals.Events;
import in.visionceg.vision2k17.Datas.EventsData;
import in.visionceg.vision2k17.Datas.Home;
import in.visionceg.vision2k17.Individuals.StudentAmbassadors;
import in.visionceg.vision2k17.R;
import in.visionceg.vision2k17.Individuals.Workshops;

/**
 * Created by holmesvinn on 17/2/17.
 */

public class HomeAdapter extends RecyclerView.Adapter<HomeAdapter.MyViewHolder> {

    List<Home> homeList = Collections.emptyList();
    private String url, jsdata;
    Context context;


    public HomeAdapter(List<Home> homeList, Context context) {
        this.homeList = homeList;
        this.context = context;
    }


    @Override
    public HomeAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.home_fragment_list_row, parent, false);
        MyViewHolder holder = new MyViewHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(HomeAdapter.MyViewHolder holder, final int position) {
        holder.title.setText(homeList.get(position).getTitle());
        holder.image.setImageResource(homeList.get(position).getImageid());
        holder.image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                homeList.remove(position);
                notifyItemRemoved(position);
                notifyItemRangeChanged(position,homeList.size());
            }
        });


    }

    @Override
    public int getItemCount() {
        return homeList.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView title;
        ImageView image;

        public MyViewHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.title);
            image = (ImageView) itemView.findViewById(R.id.homeimage);

            itemView.setOnClickListener(this);

        }


        @Override
        public void onClick(View v) {

            int position = getAdapterPosition();
            switch (position) {
                case 0:
                    Intent intent = new Intent(context, Events.class);
                    context.startActivity(intent);
                    break;

                case 1:
                    Intent intent1 = new Intent(context, Workshops.class);
                    context.startActivity(intent1);
                    break;

                case 2:
                    Intent intent3 = new Intent(context,Accomodation.class);
                    context.startActivity(intent3);
                    break;


                case 3:
                    Intent intent2 = new Intent(context,Contacts.class);
                    context.startActivity(intent2);
                    break;

                case 4:
                    Intent intent4 = new Intent(context, StudentAmbassadors.class);
                    context.startActivity(intent4);
                    break;

            }
        }
    }


    private class downloadTask extends AsyncTask<String, String, String> {

        private ProgressDialog progressDialog = new ProgressDialog(context);


        @Override
        protected String doInBackground(String... params) {
            HttpURLConnection connection = null;

            BufferedReader reader = null;

            try {
                URL url = new URL(params[0]);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();

                InputStream stream = connection.getInputStream();

                reader = new BufferedReader(new InputStreamReader(stream));

                StringBuffer buffer = new StringBuffer();

                String line = " ";

                while ((line = reader.readLine()) != null) {

                    buffer.append(line);
                }


                jsdata = buffer.toString();
                return null;
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return jsdata;
        }


        @Override
        protected void onPreExecute() {


            this.progressDialog.setMessage("Loading Data");
            progressDialog.show();
        }

        @Override
        protected void onPostExecute(String s) {
            s = jsdata;
            progressDialog.dismiss();
            List<EventsData> data = new ArrayList<>();

            Toast.makeText(context, s, Toast.LENGTH_LONG).show();
        }

    }
}

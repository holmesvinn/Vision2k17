package in.visionceg.vision2k17.Adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import java.util.List;

import in.visionceg.vision2k17.R;
import in.visionceg.vision2k17.Datas.WorkshopsData;
import in.visionceg.vision2k17.Individual.Workshops_individual;

/**
 * Created by holmesvinn on 23/2/17.
 */

public class WorkshopsAdapter extends RecyclerView.Adapter<WorkshopsAdapter.CustomViewHolder> {


    List<WorkshopsData> workshopsDatas;
    Context context;

    public WorkshopsAdapter(Context context, List<WorkshopsData> workshopsDatas) {
        this.context = context;
        this.workshopsDatas = workshopsDatas;
    }

    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder()

                .cacheInMemory(true)
                .cacheOnDisk(true)

                .build();
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context.getApplicationContext())

                .defaultDisplayImageOptions(defaultOptions)

                .build();
        ImageLoader.getInstance().init(config);

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.workshops_list_row, null);
        WorkshopsAdapter.CustomViewHolder viewHolder = new WorkshopsAdapter.CustomViewHolder(view);
        return viewHolder;

    }

    @Override
    public void onBindViewHolder(final CustomViewHolder holder, int position) {

        WorkshopsData workshopsData = workshopsDatas.get(position);
        ImageLoader.getInstance().displayImage(workshopsData.getImgurl(), holder.imageView, new ImageLoadingListener() {

            @Override
            public void onLoadingStarted(String imageUri, View view) {
                holder.imageView.setImageResource(R.drawable.nonet);

            }

            @Override
            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {

            }

            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {

                holder.imageView.setImageBitmap(loadedImage);

            }

            @Override
            public void onLoadingCancelled(String imageUri, View view) {

            }
        });
        holder.textView.setText(Html.fromHtml(workshopsData.getWorkname()));

    }

    @Override
    public int getItemCount() {
        return (null != workshopsDatas ? workshopsDatas.size() : 0);
    }

    public class CustomViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private ImageView imageView;
        private TextView textView;

        public CustomViewHolder(View itemView) {

            super(itemView);
            this.imageView = (ImageView) itemView.findViewById(R.id.thumbnail_work);
            this.textView = (TextView) itemView.findViewById(R.id.title_work);
            itemView.setOnClickListener(this);


        }

        @Override
        public void onClick(View v) {

            int position = getAdapterPosition();
            String workshop_name = workshopsDatas.get(position).getWorkname();
            String introduction = workshopsDatas.get(position).getIntroduction();
            String course = workshopsDatas.get(position).getCourse();
            String details = workshopsDatas.get(position).getDetails();
            String contact = workshopsDatas.get(position).getContact();
            String imgurll = workshopsDatas.get(position).getImgurl();

            Intent intetn = new Intent(context.getApplicationContext(), Workshops_individual.class);
            intetn.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            Bundle b = new Bundle();
            b.putString("work_name", workshop_name);
            b.putString("introduction", introduction);
            b.putString("course", course);
            b.putString("details", details);
            b.putString("contact", contact);
            b.putString("imgurl", imgurll);
            intetn.putExtras(b);
            context.getApplicationContext().startActivity(intetn);


        }
    }
}

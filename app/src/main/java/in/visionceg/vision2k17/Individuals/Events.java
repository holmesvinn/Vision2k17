package in.visionceg.vision2k17.Individuals;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import in.visionceg.vision2k17.Adapters.EventsAdapter;
import in.visionceg.vision2k17.Datas.EventsData;
import in.visionceg.vision2k17.R;
import io.paperdb.Paper;

/**
 * Created by holmesvinn on 22/2/17.
 */
public class Events extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {

    private List<EventsData> eventsDatas;
    private RecyclerView recyclerView;
    private EventsAdapter adapter;
    private ProgressBar progressBar;
    private SwipeRefreshLayout swipeRefreshLayout;
    String url;


    public int size = 0;

    final String PREFS_NAME = "MyPrefsFile";





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.events);

        Paper.init(getApplicationContext());




        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe1);

        swipeRefreshLayout.setColorSchemeColors(getResources().getColor(android.R.color.holo_blue_dark),
                getResources().getColor(android.R.color.holo_red_dark),
                getResources().getColor(android.R.color.holo_blue_dark),
                getResources().getColor(android.R.color.holo_orange_dark));

        swipeRefreshLayout.setOnRefreshListener(this);



        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        progressBar = (ProgressBar) findViewById(R.id.progress_bar);

        url = "http://vision2k17.azurewebsites.net/technical";




        connectivity_check();




    }

    private void connectivity_check() {

        ConnectivityManager cm =
                (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();

        if(isConnected) {
            new DownloadTask().execute(url);
        }

        else {

            Paper.book().write("jstring",LoadfromAsset());
            new DownloadTask().execute(url);
        }

    }

    private String LoadfromAsset() {
        String json  = null;
        try {

            InputStream is = getAssets().open("events.json");

            int size = is.available();

            byte[] buffer = new byte[size];

            is.read(buffer);

            is.close();

            json = new String(buffer, "UTF-8");


        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }


        return json.toString();
    }

    @Override
    public void onRefresh() {
        connectivity_check();
    }


    private class DownloadTask extends AsyncTask<String,Void,Integer> {


        private static final String TAG = "Some Value";

        @Override
        protected Integer doInBackground(String... params) {
            Paper.init(getApplicationContext());
            Integer result = 0;
            HttpURLConnection urlConnection;
            try {
                URL url = new URL(params[0]);
                urlConnection = (HttpURLConnection) url.openConnection();
                int statusCode = urlConnection.getResponseCode();

                // 200 represents HTTP OK
                if (statusCode == 200) {
                    BufferedReader r = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                    StringBuilder response = new StringBuilder();
                    String line;
                    while ((line = r.readLine()) != null) {
                        response.append(line);
                    }



                    parseResult(response.toString());

                    result = 1; // Successful
                } else {



                    result = 0;



                    //"Failed to fetch data!";
                }
            } catch (Exception e) {
                Log.d(TAG, e.getLocalizedMessage());
            }
            return result; //"Failed to fetch data!";
        }


        @Override
        protected void onPreExecute() {
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected void onPostExecute(Integer integer) {
            progressBar.setVisibility(View.GONE);

            if (integer == 1) {



                recyclerView.setHasFixedSize(true);
                adapter = new EventsAdapter(getApplicationContext(), eventsDatas);
                recyclerView.setAdapter(adapter);
                recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));

            } else {

                String jstring = Paper.book().read("jstring");
                parseResult(jstring);
                recyclerView.setHasFixedSize(true);
                adapter = new EventsAdapter(getApplicationContext(), eventsDatas);
                recyclerView.setAdapter(adapter);
                recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
                Toast.makeText(getApplicationContext(), "Network Error", Toast.LENGTH_LONG).show();


            }
        }
    }



    private void parseResult(String s) {







        try {
            JSONArray response = new JSONArray(s);
            JSONObject val = response.getJSONObject(0);
            JSONArray posts = val.getJSONArray("technical_events");
            eventsDatas = new ArrayList<>();

            for (int i = 0; i < posts.length(); i++) {
                JSONObject post = posts.optJSONObject(i);
                EventsData item = new EventsData();
                item.setEventname(post.optString("eventname"));
                item.setImageurl(post.optString("imageurl"));
                item.setCategory(post.optString("category"));
                item.setDescription(post.optString("description"));
                item.setRules(post.optString("rules"));
                item.setVenue(post.optString("venue"));
                item.setContact(post.optString("contact"));
                eventsDatas.add(item);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }



    }
}
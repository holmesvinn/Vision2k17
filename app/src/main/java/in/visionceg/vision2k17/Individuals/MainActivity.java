package in.visionceg.vision2k17.Individuals;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import in.visionceg.vision2k17.AboutActivity;
import in.visionceg.vision2k17.Fragments.HomeFragment;
import in.visionceg.vision2k17.Fragments.ProfileFragment;
import in.visionceg.vision2k17.Fragments.UpdatesFragment;
import in.visionceg.vision2k17.R;
import io.paperdb.Paper;

public class MainActivity extends AppCompatActivity {
     public Button Home, Updates, Profile;

    Fragment fragment = null;
    String frag = null;


    public Drawable homedrk,home,heart,heartdark,usr,usrdrk;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

       Intent intent = getIntent();
        if(intent.hasExtra("hello"))
        {
            frag = "value";
        }



        homedrk = getBaseContext().getResources().getDrawable(R.drawable.homedrk24);
        home = getBaseContext().getResources().getDrawable(R.drawable.home24);
        heart = getBaseContext().getResources().getDrawable(R.drawable.heart24);
        heartdark = getBaseContext().getResources().getDrawable(R.drawable.heartdrk24);
        usr = getBaseContext().getResources().getDrawable(R.drawable.user24);
        usrdrk = getBaseContext().getResources().getDrawable(R.drawable.userdark24);


        Home = (Button) findViewById(R.id.home);

        Updates = (Button) findViewById(R.id.updates);

        Profile = (Button) findViewById(R.id.profile);



        if(frag =="value")
        {
            fragment = new UpdatesFragment();
            FragmentManager manager = getSupportFragmentManager();
            FragmentTransaction transaction = manager.beginTransaction();
            transaction.replace(R.id.screen, fragment);
            transaction.commit();
            Home.setCompoundDrawablesWithIntrinsicBounds(null,null,null,home);
            Updates.setCompoundDrawablesWithIntrinsicBounds(null,null,null,heartdark);
            Profile.setCompoundDrawablesWithIntrinsicBounds(null,null,null,usr);

        }


        else {

            fragment = new HomeFragment();
            FragmentManager manager = getSupportFragmentManager();
            FragmentTransaction transaction = manager.beginTransaction();
            transaction.replace(R.id.screen, fragment);
            transaction.commit();
        }


        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {




                if (v == findViewById(R.id.home)) {

                    Home.setCompoundDrawablesWithIntrinsicBounds(null,null,null,homedrk);
                    Updates.setCompoundDrawablesWithIntrinsicBounds(null,null,null,heart);
                    Profile.setCompoundDrawablesWithIntrinsicBounds(null,null,null,usr);

                    fragment = new HomeFragment();
                }
                if (v == findViewById(R.id.updates)) {

                    Home.setCompoundDrawablesWithIntrinsicBounds(null,null,null,home);
                    Updates.setCompoundDrawablesWithIntrinsicBounds(null,null,null,heartdark);
                    Profile.setCompoundDrawablesWithIntrinsicBounds(null,null,null,usr);
                    fragment = new UpdatesFragment();
                }
                if (v == findViewById(R.id.profile)) {

                    Home.setCompoundDrawablesWithIntrinsicBounds(null,null,null,home);
                    Updates.setCompoundDrawablesWithIntrinsicBounds(null,null,null,heart);
                    Profile.setCompoundDrawablesWithIntrinsicBounds(null,null,null,usrdrk);
                    fragment = new ProfileFragment();
                }

                FragmentManager manager = getSupportFragmentManager();
                FragmentTransaction transaction = manager.beginTransaction();
                transaction.replace(R.id.screen, fragment);
                transaction.commit();

            }
        };


        Updates.setOnClickListener(listener);
        Profile.setOnClickListener(listener);
        Home.setOnClickListener(listener);


    }








    @Override
    public boolean onCreateOptionsMenu(Menu menu) {


        getMenuInflater().inflate(R.menu.menu, menu);
        return true;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id= item.getItemId();
        if(id == R.id.action_about)
        {
            Intent intent = new Intent(this, AboutActivity.class);
            startActivity(intent);

        }
        if(id == R.id.action_feedback)
        {
            String[] Email = new String[]{"holmesvinn@gmail.com"};
            Intent emailIntent = new Intent(Intent.ACTION_SEND);
            emailIntent.setData(Uri.parse("mailto:"));

            emailIntent.setType("text/plain");



            emailIntent.putExtra(Intent.EXTRA_EMAIL, Email);

            try {
                startActivity(Intent.createChooser(emailIntent, "Send mail..."));
            } catch (android.content.ActivityNotFoundException ex) {
                Toast.makeText(getApplicationContext(),
                        "There is no email client installed.", Toast.LENGTH_SHORT).show();
            }

        }


        return super.onOptionsItemSelected(item);
    }


}

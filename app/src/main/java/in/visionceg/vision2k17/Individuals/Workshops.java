package in.visionceg.vision2k17.Individuals;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatCallback;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import in.visionceg.vision2k17.Adapters.WorkshopsAdapter;
import in.visionceg.vision2k17.Datas.WorkshopsData;
import in.visionceg.vision2k17.R;
import io.paperdb.Paper;

/**
 * Created by holmesvinn on 23/2/17.
 */

public class Workshops extends AppCompatActivity implements AppCompatCallback, SwipeRefreshLayout.OnRefreshListener {


    private List<WorkshopsData> workshopsDatas;
    private RecyclerView recyclerView;
    private WorkshopsAdapter adapter;
    private ProgressBar progressBar;
    String url;
    private SwipeRefreshLayout swipeRefreshLayout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.workshops);

        Paper.init(getApplicationContext());

        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe2);

        swipeRefreshLayout.setColorSchemeColors(getResources().getColor(android.R.color.holo_blue_dark),
                getResources().getColor(android.R.color.holo_red_dark),
                getResources().getColor(android.R.color.holo_blue_dark),
                getResources().getColor(android.R.color.holo_orange_dark));

        swipeRefreshLayout.setOnRefreshListener(this);

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view_work);


        progressBar = (ProgressBar) findViewById(R.id.progress_bar_work);

         url = "http://vision2k17.azurewebsites.net/general";



        connectivity_check();





    }

    private void connectivity_check() {

        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();



        if (isConnected) {
            new DownloadTask().execute(url);
        } else {


            Paper.book().write("jstring_work", LoadfromAsset());
            new DownloadTask().execute(url);


        }
    }

    private String LoadfromAsset() {

        String jsonwork = null;
        try {

            InputStream is = getAssets().open("workshops.json");

            int size = is.available();

            byte[] buffer = new byte[size];

            is.read(buffer);

            is.close();

            jsonwork = new String(buffer, "UTF-8");


        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }


        return jsonwork.toString();
    }

    @Override
    public void onRefresh() {
        connectivity_check();
    }

    private class DownloadTask extends AsyncTask<String, Void, Integer> {


        private static final String TAG = "Some Value";

        @Override
        protected Integer doInBackground(String... params) {
            Integer result = 0;
            HttpURLConnection urlConnection;
            try {
                URL url = new URL(params[0]);
                urlConnection = (HttpURLConnection) url.openConnection();
                int statusCode = urlConnection.getResponseCode();

                // 200 represents HTTP OK
                if (statusCode == 200) {
                    BufferedReader r = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                    StringBuilder response = new StringBuilder();
                    String line;
                    while ((line = r.readLine()) != null) {
                        response.append(line);
                    }

                    Paper.book().write("jstring_work", response.toString());

                    parseResult(response.toString());

                    result = 1; // Successful
                } else {


                    result = 0;


                    //"Failed to fetch data!";
                }
            } catch (Exception e) {
                Log.d(TAG, e.getLocalizedMessage());
            }
            return result; //"Failed to fetch data!";
        }


        @Override
        protected void onPreExecute() {
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected void onPostExecute(Integer integer) {
            progressBar.setVisibility(View.GONE);

            if (integer == 1) {


                recyclerView.setHasFixedSize(true);
                adapter = new WorkshopsAdapter(getApplicationContext(), workshopsDatas);
                recyclerView.setAdapter(adapter);
                recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));

            } else {


                String jstring = Paper.book().read("jstring_work");
                parseResult(jstring);
                recyclerView.setHasFixedSize(true);
                adapter = new WorkshopsAdapter(getApplicationContext(), workshopsDatas);
                recyclerView.setAdapter(adapter);
                recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
                Toast.makeText(getApplicationContext(), "Network Error", Toast.LENGTH_SHORT).show();


            }
        }
    }


    private void parseResult(String s) {


        try {
            JSONArray response = new JSONArray(s);
            JSONObject val = response.getJSONObject(0);
            JSONArray posts = val.getJSONArray("workshops");
            workshopsDatas = new ArrayList<>();

            for (int i = 0; i < posts.length(); i++) {
                JSONObject post = posts.optJSONObject(i);
                WorkshopsData item = new WorkshopsData();
                item.setWorkname(post.optString("workshop_name"));
                item.setCourse(post.optString("course"));
                item.setDetails(post.optString("details"));
                item.setContact(post.optString("contact"));
                item.setIntroduction(post.optString("introduction"));
                item.setImgurl(post.optString("imageurl"));
                workshopsDatas.add(item);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }
}

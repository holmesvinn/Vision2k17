package in.visionceg.vision2k17.Individuals;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.thefinestartist.finestwebview.FinestWebView;

import in.visionceg.vision2k17.R;
import io.paperdb.Paper;

/**
 * Created by holmesvinn on 24/2/17.
 */
public class Accomodation extends Activity {

    Button priyamail,priyanum,naveenmail,naveennum,dineshmail,dineshnum;
    String[] Email;
    private Button register,form;
    String url;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.accomodation);
        priyamail = (Button) findViewById(R.id.priyamail);
        priyanum = (Button) findViewById(R.id.priyanum);
        naveenmail = (Button) findViewById(R.id.naveenmail);
        naveennum = (Button) findViewById(R.id.naveennum);
        dineshmail =(Button) findViewById(R.id.dineshmail);
        dineshnum = (Button) findViewById(R.id.dineshnum);
        register = (Button) findViewById(R.id.accd_reg);
        form = (Button) findViewById(R.id.accrd_form);

        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                connectivity_check2();

            }
        });

        form.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                url = "https://drive.google.com/file/d/0B_d3t8Ugb4UZYjhURnVHUmhyVUU/view";

                connectivity_check();


            }
        });

        priyamail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Email = new String[]{"priya22.pr@gmail.com"};
                Intent emailIntent = new Intent(Intent.ACTION_SEND);
                emailIntent.setData(Uri.parse("mailto:"));
                emailIntent.setType("text/plain");



                emailIntent.putExtra(Intent.EXTRA_EMAIL, Email);

                try {
                    startActivity(Intent.createChooser(emailIntent, "Send mail..."));
                    finish();
                } catch (android.content.ActivityNotFoundException ex) {
                    Toast.makeText(getApplicationContext(),
                            "There is no email client installed.", Toast.LENGTH_SHORT).show();
                }
            }
        });

        naveenmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Email = new String[]{"naveenpude6@gmail.com"};
                Intent emailIntent = new Intent(Intent.ACTION_SEND);
                emailIntent.setData(Uri.parse("mailto:"));
                emailIntent.setType("text/plain");



                emailIntent.putExtra(Intent.EXTRA_EMAIL, Email);

                try {
                    startActivity(Intent.createChooser(emailIntent, "Send mail..."));
                    finish();
                } catch (android.content.ActivityNotFoundException ex) {
                    Toast.makeText(getApplicationContext(),
                            "There is no email client installed.", Toast.LENGTH_SHORT).show();
                }


            }
        });

        dineshmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Email = new String[]{"dineshkumarsssws@gmail.com"};
                Intent emailIntent = new Intent(Intent.ACTION_SEND);
                emailIntent.setData(Uri.parse("mailto:"));
                emailIntent.setType("text/plain");



                emailIntent.putExtra(Intent.EXTRA_EMAIL, Email);

                try {
                    startActivity(Intent.createChooser(emailIntent, "Send mail..."));
                    finish();
                } catch (android.content.ActivityNotFoundException ex) {
                    Toast.makeText(getApplicationContext(),
                            "There is no email client installed.", Toast.LENGTH_SHORT).show();
                }

            }
        });


        priyanum.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:9750245604"));
                startActivity(intent);
            }
        });


        naveennum.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:9941524958"));
                startActivity(intent);

            }
        });

        dineshnum.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:9677560311"));
                startActivity(intent);
            }
        });
    }

    private void connectivity_check2() {

        ConnectivityManager cm =
                (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();

        if(isConnected) {
            new FinestWebView.Builder(getApplicationContext()).show("https://goo.gl/forms/0fTePPT1e8f9d8e03");
        }

        else {


            Toast.makeText(getApplicationContext(),"Network Not Connected",Toast.LENGTH_LONG).show();




        }
    }

    private void connectivity_check() {

        ConnectivityManager cm =
                (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();

        if(isConnected) {
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            startActivity(browserIntent);

        }

        else {


            Toast.makeText(getApplicationContext(),"Network Not Connected",Toast.LENGTH_LONG).show();




        }
    }
}

package in.visionceg.vision2k17.Individuals;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import in.visionceg.vision2k17.R;

/**
 * Created by lenovo on 24-02-2017.
 */

public class Contacts  extends Activity {

    private TextView harimail,harinum,barathmail,barathnum,jagmail,jagnum,shobmail,shobnum,poojamail,poojanum,sridivya;
    String[] Email;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.contacts_people);
        harimail = ( TextView) findViewById(R.id.harimail);
        harinum = (TextView) findViewById(R.id.harinum);
        barathmail = (TextView) findViewById(R.id.bharathmail);
        barathnum = (TextView) findViewById(R.id.bharathnum);
        jagmail = (TextView) findViewById(R.id.jagmail);
        jagnum = (TextView) findViewById(R.id.jagnum);
        shobmail = (TextView) findViewById(R.id.shobmail);
        shobnum = (TextView) findViewById(R.id.shobnum);
        poojamail = (TextView) findViewById(R.id.poojamail);
        poojanum = (TextView) findViewById(R.id.poojanum);

        sridivya = (TextView) findViewById(R.id.sridivyamail);

        harimail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Email = new String[]{harimail.getText().toString()};
                Intent emailIntent = new Intent(Intent.ACTION_SEND);
                emailIntent.setData(Uri.parse("mailto:"));
                emailIntent.setType("text/plain");



                emailIntent.putExtra(Intent.EXTRA_EMAIL, Email);

                try {
                    startActivity(Intent.createChooser(emailIntent, "Send mail..."));
                    finish();
                } catch (android.content.ActivityNotFoundException ex) {
                    Toast.makeText(getApplicationContext(),
                            "There is no email client installed.", Toast.LENGTH_SHORT).show();
                }
            }
        });


        harinum.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:"+harinum.getText().toString()));
                startActivity(intent);
            }
        });

        barathmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Email = new String[]{barathmail.getText().toString()+"@gmail.com"};
                Intent emailIntent = new Intent(Intent.ACTION_SEND);
                emailIntent.setData(Uri.parse("mailto:"));
                emailIntent.setType("text/plain");



                emailIntent.putExtra(Intent.EXTRA_EMAIL, Email);

                try {
                    startActivity(Intent.createChooser(emailIntent, "Send mail..."));
                    finish();
                } catch (android.content.ActivityNotFoundException ex) {
                    Toast.makeText(getApplicationContext(),
                            "There is no email client installed.", Toast.LENGTH_SHORT).show();
                }
            }
        });

        barathnum.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:"+barathnum.getText().toString()));
                startActivity(intent);

            }
        });


        jagmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Email = new String[]{jagmail.getText().toString()+"@gmail.com"};
                Intent emailIntent = new Intent(Intent.ACTION_SEND);
                emailIntent.setData(Uri.parse("mailto:"));
                emailIntent.setType("text/plain");



                emailIntent.putExtra(Intent.EXTRA_EMAIL, Email);

                try {
                    startActivity(Intent.createChooser(emailIntent, "Send mail..."));
                    finish();
                } catch (android.content.ActivityNotFoundException ex) {
                    Toast.makeText(getApplicationContext(),
                            "There is no email client installed.", Toast.LENGTH_SHORT).show();
                }

            }
        });

        jagnum.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:"+jagnum.getText().toString()));
                startActivity(intent);
            }
        });


        sridivya.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Email = new String[]{sridivya.getText().toString()+"@gmail.com"};
                Intent emailIntent = new Intent(Intent.ACTION_SEND);
                emailIntent.setData(Uri.parse("mailto:"));
                emailIntent.setType("text/plain");



                emailIntent.putExtra(Intent.EXTRA_EMAIL, Email);

                try {
                    startActivity(Intent.createChooser(emailIntent, "Send mail..."));
                    finish();
                } catch (android.content.ActivityNotFoundException ex) {
                    Toast.makeText(getApplicationContext(),
                            "There is no email client installed.", Toast.LENGTH_SHORT).show();
                }

            }
        });



        shobmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Email = new String[]{shobmail.getText().toString()+"@gmail.com"};
                Intent emailIntent = new Intent(Intent.ACTION_SEND);
                emailIntent.setData(Uri.parse("mailto:"));
                emailIntent.setType("text/plain");



                emailIntent.putExtra(Intent.EXTRA_EMAIL, Email);

                try {
                    startActivity(Intent.createChooser(emailIntent, "Send mail..."));
                    finish();
                } catch (android.content.ActivityNotFoundException ex) {
                    Toast.makeText(getApplicationContext(),
                            "There is no email client installed.", Toast.LENGTH_SHORT).show();
                }
            }
        });


        shobnum.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:"+shobnum.getText().toString()));
                startActivity(intent);

            }
        });


        poojamail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Email = new String[]{poojamail.getText().toString()};
                Intent emailIntent = new Intent(Intent.ACTION_SEND);
                emailIntent.setData(Uri.parse("mailto:"));
                emailIntent.setType("text/plain");



                emailIntent.putExtra(Intent.EXTRA_EMAIL, Email);

                try {
                    startActivity(Intent.createChooser(emailIntent, "Send mail..."));
                    finish();
                } catch (android.content.ActivityNotFoundException ex) {
                    Toast.makeText(getApplicationContext(),
                            "There is no email client installed.", Toast.LENGTH_SHORT).show();
                }

            }
        });

        poojanum.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:"+poojanum.getText().toString()));
                startActivity(intent);

            }
        });
    }
}

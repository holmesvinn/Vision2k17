package in.visionceg.vision2k17.Individuals;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.thefinestartist.finestwebview.FinestWebView;

import in.visionceg.vision2k17.R;

public class StudentAmbassadors extends AppCompatActivity {
    private String[] Email;

    private TextView vision,student;
    private Button sanmail,sannum,nanmail,nannum,srimail,srinum;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_ambassadors);
        vision = (TextView) findViewById(R.id.vreg);
        student = (TextView) findViewById(R.id.sareg);
        sanmail = (Button) findViewById(R.id.sanmail);
        sannum = (Button) findViewById(R.id.sanphone);
        nanmail = (Button) findViewById(R.id.nanmail);
        nannum = (Button) findViewById(R.id.nannum);
        srimail = (Button) findViewById(R.id.srimail);
        srinum = (Button) findViewById(R.id.srinum);

        vision.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                connectivity_chech2();

            }
        });

        student.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                connectivity_chech1();

            }
        });
        sanmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Email = new String[]{"santhoshbalaji10f@gmail.com"};
                Intent emailIntent = new Intent(Intent.ACTION_SEND);
                emailIntent.setData(Uri.parse("mailto:"));
                emailIntent.setType("text/plain");



                emailIntent.putExtra(Intent.EXTRA_EMAIL, Email);

                try {
                    startActivity(Intent.createChooser(emailIntent, "Send mail..."));
                    finish();
                } catch (android.content.ActivityNotFoundException ex) {
                    Toast.makeText(getApplicationContext(),
                            "There is no email client installed.", Toast.LENGTH_SHORT).show();
                }


            }
        });


        sannum.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:9444079771"));
                startActivity(intent);
            }
        });

        nanmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Email = new String[]{"nandhini0856@gmail.com"};
                Intent emailIntent = new Intent(Intent.ACTION_SEND);
                emailIntent.setData(Uri.parse("mailto:"));
                emailIntent.setType("text/plain");



                emailIntent.putExtra(Intent.EXTRA_EMAIL, Email);

                try {
                    startActivity(Intent.createChooser(emailIntent, "Send mail..."));
                    finish();
                } catch (android.content.ActivityNotFoundException ex) {
                    Toast.makeText(getApplicationContext(),
                            "There is no email client installed.", Toast.LENGTH_SHORT).show();
                }



            }
        });
        nannum.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:9698726763"));
                startActivity(intent);

            }
        });

        srimail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Email = new String[]{"srimathi1312@gmail.com"};
                Intent emailIntent = new Intent(Intent.ACTION_SEND);
                emailIntent.setData(Uri.parse("mailto:"));
                emailIntent.setType("text/plain");



                emailIntent.putExtra(Intent.EXTRA_EMAIL, Email);

                try {
                    startActivity(Intent.createChooser(emailIntent, "Send mail..."));
                    finish();
                } catch (android.content.ActivityNotFoundException ex) {
                    Toast.makeText(getApplicationContext(),
                            "There is no email client installed.", Toast.LENGTH_SHORT).show();
                }



            }
        });

        srinum.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:9487768493"));
                startActivity(intent);

            }
        });

    }

    private void connectivity_chech2() {

        ConnectivityManager cm =
                (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();

        if(isConnected) {
            new FinestWebView.Builder(getApplicationContext()).show("http://visionceg.in");
        }

        else {


            Toast.makeText(getApplicationContext(),"Network Not Connected",Toast.LENGTH_LONG).show();




        }
    }

    private void connectivity_chech1() {

        ConnectivityManager cm =
                (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();

        if(isConnected) {
            new FinestWebView.Builder(getApplicationContext()).show("https://goo.gl/forms/yohzm0URfG7vorzg1");
        }

        else {


            Toast.makeText(getApplicationContext(),"Network Not Connected",Toast.LENGTH_LONG).show();




        }
    }
}

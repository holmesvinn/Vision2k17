package in.visionceg.vision2k17;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import in.visionceg.vision2k17.Individuals.MainActivity;


public class SplashScreen extends AppCompatActivity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        Thread mythread  = new Thread()
        {

            @Override
            public void run() {
                try {
                    sleep(1000);

                    Intent intent = new Intent(getApplication(), MainActivity.class);
                    startActivity(intent);
                    finish();
                }
                catch (Exception e)
                {
                 e.printStackTrace();
                }


            }
        };

        mythread.start();
    }


}

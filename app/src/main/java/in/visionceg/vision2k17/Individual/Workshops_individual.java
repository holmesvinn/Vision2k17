package in.visionceg.vision2k17.Individual;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatCallback;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.view.ActionMode;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.thefinestartist.finestwebview.FinestWebView;

import in.visionceg.vision2k17.R;

public class Workshops_individual extends Activity implements AppCompatCallback {



    private Toolbar toolbar;
    private ProgressBar prog;
    private AppCompatDelegate delegate;

    private String workshop_name, introducction, coursse, detaiils, contaccts, imgurrl;
    private ProgressBar progressBar;
    private ImageView imageView;
    private TextView  intro, course, details, contacts,link;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        delegate = AppCompatDelegate.create(this,this);
        delegate.onCreate(savedInstanceState);
        delegate.setContentView(R.layout.activity_workshops_individual);
        toolbar = (Toolbar) findViewById(R.id.toolbar_work);
        delegate.setSupportActionBar(toolbar);
        delegate.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        delegate.getSupportActionBar().setDisplayShowHomeEnabled(true);
        link = (TextView) findViewById(R.id.linnnk);
        progressBar = (ProgressBar) findViewById(R.id.progress_bar_work2);
        intro = (TextView) findViewById(R.id.introduction);
        course = (TextView) findViewById(R.id.course);
        details = (TextView) findViewById(R.id.details);
        contacts = (TextView) findViewById(R.id.contact_work);
        imageView = (ImageView) findViewById(R.id.work_image);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        toolbar.setTitle(null);

        DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder()

                .cacheInMemory(true)
                .cacheOnDisk(true)

                .build();
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getApplicationContext())

                .defaultDisplayImageOptions(defaultOptions)

                .build();
        ImageLoader.getInstance().init(config);

        Bundle b = getIntent().getExtras();
        workshop_name = b.getString("work_name");
        introducction = b.getString("introduction");
        coursse = b.getString("course");
        detaiils = b.getString("details");
        contaccts = b.getString("contact");
        imgurrl = b.getString("imgurl");
        showdata();

    }

    private void showdata() {

       toolbar.setTitle(workshop_name);
        intro.append(String.valueOf(introducction));
        course.append(String.valueOf(coursse));
        details.append(String.valueOf(detaiils));
        contacts.append(String.valueOf(contaccts));

            link.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {


                    connectivity();


                }
            });



        ImageLoader.getInstance().displayImage(imgurrl, imageView, new ImageLoadingListener() {

            @Override
            public void onLoadingStarted(String imageUri, View view) {
                progressBar.setVisibility(View.VISIBLE);
            }

            @Override
            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {

                progressBar.setVisibility(View.GONE);
                Toast.makeText(getApplicationContext(), "Image Loading Failed", Toast.LENGTH_LONG).show();

            }

            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                progressBar.setVisibility(View.GONE);

            }

            @Override
            public void onLoadingCancelled(String imageUri, View view) {
                progressBar.setVisibility(View.GONE);

            }
        });

    }

    private void connectivity() {

        ConnectivityManager cm =
                (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();

        if(isConnected) {

            if(workshop_name == "BRAINWAVE CONTROLLED ROBOTICS")
            {

                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://visionceg.in/inner/bw.html"));
                startActivity(browserIntent);
            }
            else if(workshop_name == "TEXAS INSTRUMENTS MSP430")
            {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://visionceg.in/inner/ti.html"));
                startActivity(browserIntent);
            }

            else if(workshop_name == "BSNL-EVOLUTION OF CELLULAR TECHNOLOGY")
            {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://visionceg.in/inner/bsnl.html"));
                startActivity(browserIntent);
            }
            else
            {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://visionceg.in/inner/workshop.html"));
                startActivity(browserIntent);
            }
        }

        else {


            Toast.makeText(getApplicationContext(),"Network Not Connected",Toast.LENGTH_LONG).show();




        }
    }

    @Override
    public void onSupportActionModeStarted(ActionMode mode) {

    }

    @Override
    public void onSupportActionModeFinished(ActionMode mode) {

    }

    @Nullable
    @Override
    public ActionMode onWindowStartingSupportActionMode(ActionMode.Callback callback) {
        return null;
    }
}

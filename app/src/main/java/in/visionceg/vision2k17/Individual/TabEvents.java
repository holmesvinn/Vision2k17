package in.visionceg.vision2k17.Individual;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.graphics.Bitmap;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatCallback;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.view.ActionMode;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import java.util.ArrayList;
import java.util.List;

import in.visionceg.vision2k17.R;

public class TabEvents extends Activity implements AppCompatCallback{

    private ImageView image;
    public TextView  description, ruless, venue, contactss;

    private String event_name, categorry, desscription, rulles, vennue, contacct, imagge;

    private Toolbar toolbar;
    private ProgressBar prog;
    private AppCompatDelegate delegate;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        delegate = AppCompatDelegate.create(this,this);
        delegate.onCreate(savedInstanceState);
        delegate.setContentView(R.layout.activity_tab_events);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        delegate.setSupportActionBar(toolbar);
        delegate.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        delegate.getSupportActionBar().setDisplayShowHomeEnabled(true);

        prog = (ProgressBar) findViewById(R.id.progress_bar2);
        description = (TextView) findViewById(R.id.description);
        ruless = (TextView) findViewById(R.id.rules);
        venue = (TextView) findViewById(R.id.venue);
        contactss = (TextView) findViewById(R.id.contact);
        image = (ImageView) findViewById(R.id.event_image);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        toolbar.setTitle(null);




        Bundle b = getIntent().getExtras();
        event_name = b.getString("name");
        categorry = b.getString("category");
        desscription = b.getString("description");
        rulles = b.getString("rules");
        vennue = b.getString("venue");
        contacct = b.getString("contact");
        imagge = b.getString("imageurl");

        displaydata();


    }

    private void displaydata() {

        toolbar.setTitle(event_name);
        description.append(String.valueOf(desscription));
        ruless.append(String.valueOf(rulles));
        venue.append(String.valueOf(vennue));
        contactss.append(String.valueOf(contacct));

        ImageLoader.getInstance().displayImage(imagge, image, new ImageLoadingListener() {

            @Override
            public void onLoadingStarted(String imageUri, View view) {
                prog.setVisibility(View.VISIBLE);
            }

            @Override
            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {

                prog.setVisibility(View.GONE);
                Toast.makeText(getApplicationContext(), "Image Loading Failed", Toast.LENGTH_LONG).show();

            }

            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                prog.setVisibility(View.GONE);

            }

            @Override
            public void onLoadingCancelled(String imageUri, View view) {
                prog.setVisibility(View.GONE);

            }
        });


    }


    @Override
    public void onSupportActionModeStarted(ActionMode mode) {

    }

    @Override
    public void onSupportActionModeFinished(ActionMode mode) {

    }

    @Nullable
    @Override
    public ActionMode onWindowStartingSupportActionMode(ActionMode.Callback callback) {
        return null;
    }

    private class ViewPagerAdapter  extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();





        public ViewPagerAdapter(FragmentManager fragmentManager) {
            super(fragmentManager);
        }


        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }




}

package in.visionceg.vision2k17.Fragments;

import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import in.visionceg.vision2k17.Adapters.HomeAdapter;
import in.visionceg.vision2k17.Datas.Home;
import in.visionceg.vision2k17.R;

/**
 * Created by holmesvinn on 17/2/17.
 */
public class HomeFragment extends android.support.v4.app.Fragment {

    private List<Home> homeList = new ArrayList<>();
    private RecyclerView recyclerView;
    private HomeAdapter mAdapter;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup viewGroup, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.home_fragment, viewGroup, false);


        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        mAdapter = new HomeAdapter(homeList, getContext());
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);

        prepareData();


        return view;

    }

    private void prepareData() {
        Home events = new Home(R.mipmap.events, "EVENTS");
        homeList.add(events);

        Home workshops = new Home(R.mipmap.workshops, "WORKSHOPS");
        homeList.add(workshops);


        Home Hospitality = new Home(R.mipmap.motel, "ACCOMODATION");
        homeList.add(Hospitality);


        Home Contacts = new Home(R.mipmap.contacts, "CONTACTS");
        homeList.add(Contacts);


        Home studentam = new Home(R.mipmap.sa, "STUDENT AMBASSADORS");
        homeList.add(studentam);




    }
}

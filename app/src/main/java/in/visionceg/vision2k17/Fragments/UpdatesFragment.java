package in.visionceg.vision2k17.Fragments;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.thefinestartist.Base;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import in.visionceg.vision2k17.Adapters.UpdatesAdapter;
import in.visionceg.vision2k17.Datas.EventsData;
import in.visionceg.vision2k17.Datas.UpdatesData;
import in.visionceg.vision2k17.R;
import io.paperdb.Paper;

import static com.thefinestartist.utils.content.ContextUtil.getApplicationContext;
import static com.thefinestartist.utils.service.ServiceUtil.getSystemService;

/**
 * Created by holmesvinn on 17/2/17.
 */
public class UpdatesFragment extends android.support.v4.app.Fragment {
    
    private RecyclerView recyclerView;
    private ProgressBar progressBar;
    private String url;
    List<UpdatesData> updatesdata;
    private UpdatesAdapter adapter;
    private TextView tview;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup viewGroup, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.updates_fragment, viewGroup, false);
        Base.initialize(getContext());
        Paper.init(getContext());
        Paper.book().read("ustring",null);
        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view_update);
        progressBar = (ProgressBar) view.findViewById(R.id.progress_bar_update);
        url = "http://vision2k17.azurewebsites.net/online";
        connectivity_check();
        return view;

    }

    private void connectivity_check() {

        ConnectivityManager cm =
                (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();

        if(isConnected) {

            new DownloadTask().execute(url);


        }

        else {

            if(Paper.book().read("ustring")!=null)
            {

                String hjfkjd = Paper.book().read("ustring");
                new DownloadTask().execute(hjfkjd);
            }
            else {
                Toast.makeText(getContext(), "Network Error", Toast.LENGTH_LONG).show();
                progressBar.setVisibility(View.GONE);
            }
        }
    }

    private class DownloadTask  extends AsyncTask<String, String, Integer> {

        private static final String TAG = "Some Value";

        @Override
        protected Integer doInBackground(String... params) {
            Integer result = 0;
            HttpURLConnection urlConnection;
            try {
                URL url = new URL(params[0]);
                urlConnection = (HttpURLConnection) url.openConnection();
                int statusCode = urlConnection.getResponseCode();

                // 200 represents HTTP OK
                if (statusCode == 200) {
                    BufferedReader r = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                    StringBuilder response = new StringBuilder();
                    String line;
                    while ((line = r.readLine()) != null) {
                        response.append(line);
                    }
                    Paper.book().write("ustring",response.toString());
                    parseResult(response.toString());
                    result = 1; // Successful
                } else {



                    result = 0;



                    //"Failed to fetch data!";
                }
            } catch (Exception e) {
                Log.d(TAG, e.getLocalizedMessage());
            }
            return result; //"Failed to fetch data!";
        }




        @Override
        protected void onPreExecute() {

            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected void onPostExecute(Integer integer) {


            progressBar.setVisibility(View.GONE);

            if (integer == 1) {



                recyclerView.setHasFixedSize(true);
                adapter = new UpdatesAdapter(getContext(),updatesdata);
                recyclerView.setAdapter(adapter);
                recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

            } else {

                String jstring = Paper.book().read("ustring");
                parseResult(jstring);
                recyclerView.setHasFixedSize(true);
                adapter = new UpdatesAdapter(getContext(), updatesdata);
                recyclerView.setAdapter(adapter);
                recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
                Toast.makeText(getApplicationContext(), "Network Error", Toast.LENGTH_LONG).show();


            }
        }


    }

    private void parseResult(String some) {

        try {
            JSONArray response = new JSONArray(some);
            JSONObject val = response.getJSONObject(0);
            JSONArray posts = val.getJSONArray("updates");
            updatesdata = new ArrayList<>();

            for (int i = 0; i < posts.length(); i++) {
                JSONObject post = posts.optJSONObject(i);
                UpdatesData item = new UpdatesData();
                item.setName(post.optString("name"));
                item.setTime(post.optString("time"));
                item.setPhone(post.optString("phone"));
                item.setPerson(post.optString("person"));
                updatesdata.add(item);
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}

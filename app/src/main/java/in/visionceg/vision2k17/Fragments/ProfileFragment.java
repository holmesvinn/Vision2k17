package in.visionceg.vision2k17.Fragments;

import android.app.DownloadManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.common.BitSource;
import com.journeyapps.barcodescanner.BarcodeEncoder;
import com.thefinestartist.Base;
import com.thefinestartist.finestwebview.FinestWebView;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;

import in.visionceg.vision2k17.R;
import io.paperdb.Paper;

import static com.thefinestartist.utils.content.ContextUtil.getApplicationContext;
import static com.thefinestartist.utils.service.ServiceUtil.getSystemService;


/**
 * Created by holmesvinn on 17/2/17.
 */
public class ProfileFragment extends android.support.v4.app.Fragment {

    public  EditText editText_mail,editText_id;
    private ImageView imageView;
    private Button button,idbtn;
    private ProgressDialog progressDialog;
    public String id= null;
    public String mail = null;
    public String responsestring;
    Bitmap bitmap;

    private TextView textview;
    byte[] byteArray;
    String base;
    String myid;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup viewGroup, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.profile_fragment, viewGroup, false);
        Base.initialize(getContext());


        Paper.init(getContext());
        Paper.book().read("vid",null);
        //editText_mail = (EditText) view.findViewById(R.id.mail);
        editText_id = (EditText) view.findViewById(R.id.visionid);
        imageView = (ImageView) view.findViewById(R.id.qrcode);
        button = (Button) view.findViewById(R.id.generate_button);
        textview = (TextView) view.findViewById(R.id.testing);
        idbtn = (Button) view.findViewById(R.id.getvid_btn);
        idbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               connectivity_check();
            }
        });
        progressDialog = new ProgressDialog(getContext());
        if(Paper.book().read("vid")!= null)
        {
            editText_id.setText(Paper.book().read("vid").toString());
        }

        base  = Paper.book().read("base");
        if(base!=null)
        {
           Bitmap bit =  base64ToBitmap(base);
            imageView.setImageBitmap(bit);
        }
        


        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



                id = editText_id.getText().toString();
                Paper.book().write("vid",id);
                //mail = editText_mail.getText().toString();
                if(id.isEmpty()|| id.length()>7)
                {
                    Toast.makeText(getContext(), "Enter Your VisionID", Toast.LENGTH_SHORT).show();
                }

                else {

                    //connectivity_check();

                    MultiFormatWriter multiFormatWriter = new MultiFormatWriter();
                    try {
                        BitMatrix bitMatrix = multiFormatWriter.encode(id, BarcodeFormat.QR_CODE, 500, 500);
                        BarcodeEncoder barcodeEncoder = new BarcodeEncoder();
                        bitmap = barcodeEncoder.createBitmap(bitMatrix);
                        imageView.setImageBitmap(bitmap);
                        convertndWrite(bitmap);
                    } catch (WriterException e) {
                        e.printStackTrace();
                    }

                }

            }
        });






        return view;

    }

    private void connectivity_check() {
        ConnectivityManager cm =
                (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();

        if(isConnected) {
            String url = "http://visionceg.in";
            new FinestWebView.Builder(getApplicationContext()).show(url);


        }

        else {


            Toast.makeText(getApplicationContext(),"Network Not Connected",Toast.LENGTH_LONG).show();




        }
    }

    private void convertndWrite(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
        byte[] byteArray = byteArrayOutputStream .toByteArray();
        String basee=  Base64.encodeToString(byteArray, Base64.DEFAULT);
        Paper.book().write("base",basee);
    }


    private Bitmap base64ToBitmap(String b64) {
        byte[] imageAsBytes = Base64.decode(b64.getBytes(), Base64.DEFAULT);
        return BitmapFactory.decodeByteArray(imageAsBytes, 0, imageAsBytes.length);
    }

   /* private void connectivity_check() {

        ConnectivityManager cm =
                (ConnectivityManager) getContext().getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();

        if(isConnected) {


            OkHttpClient client = new OkHttpClient();
            com.squareup.okhttp.RequestBody requestBody = new MultipartBuilder()
                    .type(MultipartBuilder.FORM) //this is what I say in my POSTman (Chrome plugin)
                    .addFormDataPart("name", "test")
                    .addFormDataPart("quality", "240p")
                    .build();
            Request request = new Request.Builder()
                    .url("http://www.visionceg.in/php/register.php")
                    .post(requestBody)
                    .build();
            try {
                Response response = client.newCall(request).execute();
                 responsestring = response.body().string();
                response.body().close();
                // do whatever you need to do with responseString
            }
            catch (Exception e) {
                e.printStackTrace();
            }

            generate(responsestring);

        }

        else {

            Toast.makeText(getContext(),"No Network",Toast.LENGTH_LONG).show();





        }
    }*/

    private void generate(String response) {

        if(response == "true") {

            MultiFormatWriter multiFormatWriter = new MultiFormatWriter();
            try {
                BitMatrix bitMatrix = multiFormatWriter.encode(id, BarcodeFormat.QR_CODE, 500, 500);
                BarcodeEncoder barcodeEncoder = new BarcodeEncoder();
                Bitmap bitmap = barcodeEncoder.createBitmap(bitMatrix);
                imageView.setImageBitmap(bitmap);
            } catch (WriterException e) {
                e.printStackTrace();
            }
        }
        else {

            textview.setText(response);

        }
    }

   /* private class checkTask extends AsyncTask<String,String,String> {

        @Override
        protected void onPreExecute() {
           progressDialog.setMessage("Validating...");
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {



            HttpAgent.get("http://visionceg.in/php/register.php")
                    .queryParams("vId",id,"mail",mail)
                    .headers("Authorization", "Basic YWRtaW46P3V5YFZhNzAw", "Content-Type", "application/x-www-form-urlencoded")
                    .goString(new StringCallback() {
                        @Override
                        protected void onDone(boolean success, String stringResults) {
                            if(success)
                            {
                                response = stringResults.toString();
                            }
                            else
                            {
                                Toast.makeText(getContext(),"Invalid Username or Password",Toast.LENGTH_LONG).show();
                            }
                        }
                    });


            return response.toString();
        }

        @Override
        protected void onPostExecute(String s) {

            progressDialog.dismiss();

            if(s=="true")
            {

                MultiFormatWriter multiFormatWriter = new MultiFormatWriter();
                try {
                    BitMatrix bitMatrix = multiFormatWriter.encode(editText_id.getText().toString(), BarcodeFormat.QR_CODE, 500, 500);
                    BarcodeEncoder barcodeEncoder = new BarcodeEncoder();
                    Bitmap bitmap = barcodeEncoder.createBitmap(bitMatrix);
                    imageView.setImageBitmap(bitmap);
                } catch (WriterException e) {
                    e.printStackTrace();
                }
            }

            else
            {
                Toast.makeText(getContext(),"Invalid Username or Password",Toast.LENGTH_LONG).show();
                textview.setText(s);

            }



        }
    }*/
}
